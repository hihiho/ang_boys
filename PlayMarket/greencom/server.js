const express = require("express");
const express2 = require("express");
const express3 = require("express");
const app = express(); //앱 생성
const app2 = express();
const app3 = express();
const app4 = express();
const path = require("path");
const bodyParser = require("body-parser");
const router =express.Router(); //라우터 생성
const router2 =express2.Router(); //라우터 생성
const router3 =express3.Router(); //라우터 생성

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const mongoose = require("mongoose"); //몽고 생성

mongoose.Promise = global.Promise;//콜벡 지옥 해결하기 위해 몽고에 promise적용

//몽고 DB 접속
mongoose.connect("mongodb+srv://green:123@cluster0-sctmh.mongodb.net/poly_ori?retryWrites=true&w=majority")
  .then(() => {
    console.log("Connected to database!");
    //initial(); // 아침 추가
  })
  .catch(() => {
    console.log("Connection failed!");
    process.exit();
  });

const cors = require("cors");
const corsOptions = {
  origin: "http://localhost:4200",
  optionsSuccessStatus: 200,
};

// app.use(cors(corsOptions));


app.use("/images", express.static(path.join("backend/images")));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});
app.use(cors(corsOptions));
// posts.js(CRUD 기능 가져옴)
const posts = require("./backend/routes/posts.js")(app,router);
// console.log("app = ",app, "router = ", router);
//console.log("1)여기는 포스트 ", posts);
// posts.js(CRUD 기능 가져옴)로부터 가져온 정보를 URL과 매핑
app.use("/api/posts", posts);

// 서버 생성후 listen함
const server = app.listen(3000, function () {
  let host = server.address().address;
  let port = server.address().port;
  console.log("App listening at http://%s:%s", host, port);
});



// customer
app2.use(bodyParser.json());
app2.use(cors(corsOptions));
const Customers = require("./backend/routes/customer.routes.js")(app2,router2);
// console.log("app2 = ",app2, "router2 = ", router2);
app2.use("/api/customers", Customers);

var server2 = app2.listen(3001, function(){
  var host = server2.address().address;
  var port = server2.address().port;
  console.log("App listening at http://%s:%s", host, port);
});
//app2.use("/api/customer", Customer);
//module.exports = app2;


// comment
app3.use(bodyParser.json());
app3.use(cors(corsOptions));
const Comments = require("./backend/routes/comment.routes.js")(app3,router3);
// console.log("app3 = ",app3, "router3 = ", router3);
app3.use("/api/comments", Comments);

var server3 = app3.listen(3002, function(){
  var host = server3.address().address;
  var port = server3.address().port;
  console.log("App listening at http://%s:%s", host, port);
});
//app3.use("/api/comment", Comment);
//module.exports = app3;
//Comment.initialize(app3);



// auth login
app4.use(bodyParser.json());
app4.use(cors(corsOptions));
// const authUser = require("./backend/routes/user")(app4);
const userRoutes = require("./backend/routes/user");

// console.log("app3 = ",app3, "router3 = ", router3);
app4.use("/api/user", userRoutes);

var server4 = app4.listen(3003, function(){
  var host = server4.address().address;
  var port = server4.address().port;
  console.log("App listening at http://%s:%s", host, port);
});
