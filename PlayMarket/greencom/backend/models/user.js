const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator"); // npm i mongoose-unique-validator


const userSchema = mongoose.Schema({
  email:{type:String, required:true, unique:true},
  password:{type:String, required:true},

  firstname:{type:String, required:true},
  lastname:{type:String, required:true},
  age:String,
  address:String,

  createDate:String
});

userSchema.plugin(uniqueValidator); // 유효성 검사 플러그인
module.exports = mongoose.model("User", userSchema);
