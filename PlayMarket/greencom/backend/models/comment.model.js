
const mongoose = require("mongoose");

const commentSchema = mongoose.Schema({

  target: {type:String, required:true},
  content: {type:String, required:true},

  createDate: {type:String, required:true},

});



module.exports = mongoose.model("Comment", commentSchema);

