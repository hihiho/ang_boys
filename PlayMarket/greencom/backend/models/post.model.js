const mongoose = require("mongoose");

//const Populate = require("../../src/app/utill/autopopulate");

const postSchema = mongoose.Schema({
  //_creator:{type: Number, ref:'Customer'}, // 글쓴이 한 사람

  title: { type: String, required: true },
  content: { type: String, required: true },
  imagePath: { type: String, required: true },

  // 추가
  createDate:{type:String, required:true},
  //modifyDate:{type:String, required:true},
  status:{type:String},
  coment:{type:String}
});

// postSchema
// .pre('findOne', Populate('author'))
// .pre('find', Populate('author'))

module.exports = mongoose.model("Post", postSchema);
