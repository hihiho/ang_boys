const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
  // _id: mongoose.Schema.Types.ObjectId, // 추가 20200624-15시

  userId: { type:String, required: true , unique : true}, // unique로 mongoDB에 primarykey 사용
  userPw: { type:String, required: true },

  firstname: { type:String, required: true }, // required === 필수값;
  lastname: { type:String, required: true },
  age: { type: Number, min: 18, max: 65, required: true }, // 정수형 타입은 입력값 제한을 이렇게 간단히 줄 수 있다.
  address: { type:String, required: false },
  createDate: { type:String, required: true }
});

customerSchema.path('firstname').validate(function (v) { // 문자열 타입은 입력값 제한을 이렇게 어렵게 줄 수 있다.
  return v.length >= 1; // 한글 성을 위해 1글자 이상
});

module.exports = mongoose.model('Customer', customerSchema);
