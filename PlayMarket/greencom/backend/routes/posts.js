const multer = require("multer");
const Post = require("../models/post.model");

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg"
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(error, "backend/images");
  },
  filename: (req, file, cb) => {
    const name = file.originalname
      .toLowerCase()
      .split(" ")
      .join("-");
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + "-" + Date.now() + "." + ext);
  }
});

module.exports = function (app, router) {
    // middleware that is specific to this router
  router.use(function timeLog (req, res, next) {
    // console.log('Time: ', Date.now());
    next();
  });

  router.post("",
    multer({ storage: storage }).single("image"),
    (req, res, next) => {
      console.log("여기는 게시글 2) " , req.url);

      const url = req.protocol + "://" + req.get("host");
      const post = new Post({
        title: req.body.title,
        content: req.body.content,
        imagePath: url + "/images/" + req.file.filename,

        // 추가
        createDate:req.body.createDate,
        //modifyDate:{type:String, required:true},
        status:req.body.status,
        coment:req.body.coment
      });
      post.save().then(createdPost => {
        res.status(201).json({
          message: "Post added successfully",
          post: {
            ...createdPost,
            id: createdPost._id
          }
        });
      });
    }
  );

  router.put(
    "/:id",
    multer({ storage: storage }).single("image"),
    (req, res, next) => {
      console.log("여기는 수정 3) ",req.id);
      let imagePath = req.body.imagePath;
      if (req.file) {
        const url = req.protocol + "://" + req.get("host");
        imagePath = url + "/images/" + req.file.filename
      }
      const post = new Post({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content,
        imagePath: imagePath,

        // 추가
        createDate:req.body.createDate,
        //modifyDate:{type:String, required:true},
        status:req.body.status,
        coment:req.body.coment
      });
      console.log(post);
      Post.updateOne({ _id: req.params.id }, post).then(result => {
        res.status(200).json({ message: "Update successful!" });
      });
    }
  );

  router.get("", (req, res, next) => {
    //console.log("여기는 전체 조회  3) ", req.url);
    Post.find().then(documents => {
      res.status(200).json({
        message: "Posts fetched successfully!",
        posts: documents
      });
    });
  });

  router.get("/:id", (req, res, next) => {
    // var currentUser = req.user; // 추가1
    Post.findById(req.params.id)
    // .populate('comments').lean() // 추가2
    .then(post => {
      if (post) {
        // res.render("posts-show", { post, currentUser }); // 추가3
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "Post not found!" });
      }
    });
  });


  router.delete("/:id", (req, res, next) => {
    Post.deleteOne({ _id: req.params.id }).then(result => {
      console.log(result);
      res.status(200).json({ message: "Post deleted!" });
    });
  });

  return router;
}
