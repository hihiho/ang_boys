const express = require("express");
const bcrypt = require("bcrypt"); // 암호화 
                                    // window-> npm i bcrypt -> require("bcrypt"); 
                                    // macos-> npm i bcryptjs --save -> require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const router = express.Router();

router.post("/signup", (req,res,next)=>{
  console.log("signup_routes");
  bcrypt.hash(req.body.password, 10).then(hash=>{
    const user = new User({
      email:req.body.email,
      password:hash,

      //추가
      firstname:req.body.firstname,
      lastname:req.body.lastname,
      age:req.body.age,
      address:req.body.address,
      createDate:req.body.createDate

    });
    user
    .save()
    .then(result=>{
      res.status(201).json({
        message:"사용자가 생성됨.",
        result:result
      });
    })
    .catch(err=>{
      res.status(500).json({
        error:err
      });
    });
  }); // bcrypt
}); // router.post

router.post("/login", (req,res,next)=>{
  let fetchedUser;
  User.findOne({email:req.body.email})
  .then(user=>{
    if(!user){
      return res.status(401).json({
        message:"인증 실패_1"
      });
    }
    fetchedUser = user;
    return bcrypt.compare(req.body.password, user.password);
  })
  .then(result=>{
    if(!result){
      return res.status(401).json({
        message:"인증 실패_2"
      });
    }
    const token = jwt.sign({
      email:fetchedUser.email, userId:fetchedUser._id
    }, "secret_this_should_be_longer", {expiresIn:"1h"});// 인증 유효시간 1hour뒤, 만기
    res.status(200).json({
      token:token,
      expiresIn:3600
    });
  })
  .catch(err=>{
    return res.status(401).json({
      message:"인증 실패_3"
    });
  });
}); // router.post
module.exports = router;
