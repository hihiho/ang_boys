module.exports = function(app3, router){

  router.use(function timeLog(req, res, next){
    next();
  });

  const comments = require('../controllers/comment.controller.js');
  console.log('Time: ', Date.now());
  console.log("routes 들어오나");

  app3.post('/api/comments', comments.create);

  app3.get('/api/comments', comments.findAll);

  app3.get('/api/comments/:commentId', comments.findOne);

  app3.put('/api/comments', comments.update);

  app3.delete('/api/comments/:commentId', comments.delete);

  return router;
}
