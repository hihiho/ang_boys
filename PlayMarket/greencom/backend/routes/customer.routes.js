module.exports = function(app2, router){

  router.use(function timeLog(req, res, next){
    next();
  });

  const customers = require('../controllers/customer.controller');
  console.log('Time: ', Date.now());
  console.log("routes 들어오나");

  app2.post('/api/customers', customers.create);

  app2.get('/api/customers', customers.findAll);

  app2.get('/api/customers/:customerId', customers.findOne);

  app2.put('/api/customers', customers.update);

  app2.delete('/api/customers/:customerId', customers.delete);

  return router;
}
