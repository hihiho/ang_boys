const Comment = require('../models/comment.model.js');


exports.create = (req, res) => {
  console.log("댓글 생성");
  console.log("들어오나 ",req.body);
    const comment = new Comment(req.body);
    console.log(req);
    comment.save()
    .then(data => {
        res.json(data);
    }).catch(err => {
        res.status(500).json({
            msg: err.message
        });
    });
};



exports.findAll = (req, res) => {
  Comment.find()
    .then(comments => {
        res.json(comments);
    }).catch(err => {
        res.status(500).send({
            msg: err.message
        });
    });
};



exports.findOne = (req, res) => {
  Comment.findById(req.params.commentId)
    .then(comment => {
        if(!comment) {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        res.json(comment);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        return res.status(500).json({
            msg: "Error retrieving comment with id " + req.params.commentId
        });
    });
};


exports.update = (req, res) => {

  Comment.findByIdAndUpdate(req.body._id, req.body, {new: true})
    .then(comment => {
        if(!comment) {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        res.json(comment);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        return res.status(500).json({
            msg: "Error updating comment with id " + req.params.commentId
        });
    });
};


exports.delete = (req, res) => {
  comment.findByIdAndRemove(req.params.commentId)
    .then(comment => {
        if(!comment) {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        res.json({msg: "comment deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).json({
                msg: "comment not found with id " + req.params.commentId
            });
        }
        return res.status(500).json({
            msg: "Could not delete comment with id " + req.params.commentId
        });
    });
};
