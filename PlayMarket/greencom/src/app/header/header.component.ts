import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { PostListComponent } from "../post-list/post-list.component";
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

@Injectable({providedIn: 'root'})
export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  private authListenerSubs: Subscription;
  searchForm: FormGroup;
  public reqSearchKeyword: string = "temp";

  constructor(private authService: AuthService, private postListComponent:PostListComponent, private router:Router){}

  ngOnInit(){
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
    .getAuthStatusListener()
    .subscribe(isAuthenticated =>{
      this.userIsAuthenticated = isAuthenticated;
    });
    this.searchForm = new FormGroup({
      search: new FormControl(null, {
        validators:[Validators.required, Validators.minLength(1)]
      })
    });
  }
  search(){
    this.reqSearchKeyword = this.searchForm.value.search;
    console.log("검색 시도 / searchKeyword : ", this.reqSearchKeyword);
    // this.router.navigate(["/post/list"]);
    // 로그인 상태에서만 검색창이 보이므로 라우팅이 필요없어보임
    this.postListComponent.flag = false;
    this.postListComponent.search(this.reqSearchKeyword);

  }
  onLogout(){
    this.authService.logout();
  }
  ngOnDestroy(){
    this.authListenerSubs.unsubscribe();
  }

}
