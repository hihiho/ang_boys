import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {
MatInputModule,
MatCardModule,
MatButtonModule,
MatToolbarModule,
MatExpansionModule,
MatProgressSpinnerModule,
MatSelectModule,
MatDialogModule,
} from "@angular/material";

import { AppComponent } from './app.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { HeaderComponent } from './header/header.component';
import { PostListComponent } from './post-list/post-list.component';
import { AppRoutingModule } from './app-routing.module';

import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { CustomerComponent } from './customer/customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
// import { LoginComponent } from './login/login.component'; // 기존 모형뿐인 로그인

//import { ComentPopupComponent } from './coment-popup/coment-popup';
import { DialogOverviewExampleDialog } from './post-list/post-list.component';

//import { NowTime } from "./nowTime.service"; // nowTime.service 자체에서 provider에 in 해서 이건 사용 안 함.

import { LoginComponent } from "./auth/login/login.component"; // auth 인증 기능을 갖는 로그인
import { SignupComponent } from "./auth/signup/signup.component";
import { AuthInterceptor } from "./auth/auth-interceptor";
import { AfterSearchPostListComponent } from './after-search-post-list/after-search-post-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerDetailsComponent,
    CustomerComponent,
    AddCustomerComponent,
	  PostCreateComponent,
    PostListComponent,
    LoginComponent,
    //ComentPopupComponent
    // DialogOverviewExample,
    DialogOverviewExampleDialog,
    HeaderComponent,
    SignupComponent,
    AfterSearchPostListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,

    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatDialogModule,
  ],
  providers: [{provide:HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true}], // 여기에 import한 class 명을 넣어주거나 && 해당 class에서 injectable로 provied에 in 해주면 된다.
  entryComponents: [  DialogOverviewExampleDialog], // 대화상자는 동적이기 때문에 앵귤러 컴파일러에게 알려주어야 함 // 여기
  bootstrap: [AppComponent]
})
export class AppModule { }
//yeob
