import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from 'rxjs';
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { Post } from "./post";

@Injectable({ providedIn:'root'})

export class PostsService {
  private posts: Post[]=[];
  private postsUpdated = new Subject<Post[]>();

  constructor(private http: HttpClient, private router: Router){ }

  getPosts() {
    console.log("getPosts");
    this.http
    .get<{ message: string; posts: any }>("http://localhost:3000/api/posts")
    .pipe(
    map(postData => {
      return postData.posts.map(post => {
        return{
          title:post.title,
          content:post.content,
          id:post._id,
          imagePath:post.imagePath,

          //추가
          createDate:post.createDate,
          status:post.status,
          coment:post.coment
        };
      });
    })
  )
  .subscribe(transformedPosts => {
    this.posts = transformedPosts;
    this.postsUpdated.next([...this.posts]);
    });
  }

  getPostUpdateListener(){
    return this.postsUpdated.asObservable();
  }

  getPost(id:string){
    console.log("getPost!!", id);
    return this.http.get<{_id:string, title:string, content:string, imagePath:string, createDate:string, status:string, coment:string}>(
      "http://localhost:3000/api/posts/" + id
      );
  }

  addPost(title:string, content:string, image:File, createDate:string, status:string, coment:string){
    console.log("addPost!!");
    const postData = new FormData();
    postData.append("title", title);
    postData.append("content", content);
    postData.append("image", image, title);

    //추가
    postData.append("createDate", createDate);
    postData.append("status", status);
    postData.append("coment", coment);
    this.http
    .post<{message:string; post:Post}>(
      "http://localhost:3000/api/posts",
      postData
    )
    .subscribe(responseData => {
      const post:Post = {
        id:responseData.post.id,
        title:title,
        content:content,
        imagePath:responseData.post.imagePath,

        //추가
        createDate:createDate,
        status:status,
        coment:coment
      };
      this.posts.push(post);
      this.postsUpdated.next([...this.posts]);
      // 이동 막고, 글 작성 후 결과화면 !hidden
      //this.router.navigate(["post/list"]); // 이동
    });
  }

  updatePost(id:string, title:string, content:string, image:File|string, createDate:string, status:string, coment:string){
    console.log("update!");
    let postData : Post|FormData;
    if(typeof image === "object"){
      postData = new FormData();
      postData.append("id", id);
      postData.append("title", title);
      postData.append("content", content);
      postData.append("image", image, title);

      //추가
      postData.append("createDate", createDate);
      postData.append("status", status);
      postData.append("coment", coment);
    }else{
      postData = {
        id:id,
        title:title,
        content:content,
        imagePath:image,

        //추가
        createDate:createDate,
        status:status,
        coment:coment
      };
    }
    this.http
    .put("http://localhost:3000/api/posts/"+id, postData)
    .subscribe(response => {
      const updatedPosts = [...this.posts];
      const oldPostIndex = updatedPosts.findIndex(p =>p.id === id);
      const post: Post = {
        id:id,
        title:title,
        content:content,
        imagePath:"",

        //추가
        createDate:createDate,
        status:status,
        coment:coment
      };
      updatedPosts[oldPostIndex] = post;
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
      //this.router.navigate(["/"]);
    });
  }

  deletePost(postId:string){
    console.log("delete!");
    this.http
    .delete("http://localhost:3000/api/posts/" + postId)
    .subscribe(() => {
      const updatedPosts = this.posts.filter(post =>post.id !== postId);
      this.posts = updatedPosts;
      this.postsUpdated.next([...this.posts]);
    });
  }

} // class PostsService
