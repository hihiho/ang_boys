import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfterSearchPostListComponent } from './after-search-post-list.component';

describe('AfterSearchPostListComponent', () => {
  let component: AfterSearchPostListComponent;
  let fixture: ComponentFixture<AfterSearchPostListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfterSearchPostListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfterSearchPostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
