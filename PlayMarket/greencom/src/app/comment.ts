export class Comment {
  _id: string;

  target: string;
  content: string;

  createDate: string;
}
