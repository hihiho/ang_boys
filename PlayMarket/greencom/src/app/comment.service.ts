import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comment } from './comment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({providedIn: 'root'})

export class CommentService {
  private commentsUrl = 'http://localhost:3002/api/comments';  // URL to web api
  constructor(
    private http: HttpClient
  ) { }

  getComments (): Observable<Comment[]> {
    console.log("getComments");
    return this.http.get<Comment[]>(this.commentsUrl);
  }

  getComment(id: string): Observable<Comment> {
    const url = this.commentsUrl+'/'+id;
    console.log("11.",`${this.commentsUrl}/${id}`);
    console.log("22",this.commentsUrl+'/'+id);
    return this.http.get<Comment>(url);
  }

  addComment (comment: Comment): Observable<Comment> {
    console.log(comment);
    console.log("addComment 들어옴~");
    console.log(this.commentsUrl, comment, httpOptions);
    console.log("히히호",this.http.post<Comment>(this.commentsUrl, comment, httpOptions));
    return this.http.post<Comment>(this.commentsUrl, comment, httpOptions);
  }

  deleteComment (comment: Comment | string): Observable<Comment> {
    const id = typeof comment === 'string' ? comment : comment._id;
    const url = `${this.commentsUrl}/${id}`;

    return this.http.delete<Comment>(url, httpOptions);
  }

  updateComment (comment: Comment): Observable<any> {
    return this.http.put(this.commentsUrl, comment, httpOptions);
  }
}
