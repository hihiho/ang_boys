import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerComponent } from './customer/customer.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';

import { PostListComponent, DialogOverviewExampleDialog } from "./post-list/post-list.component";
import { PostCreateComponent } from "./post-create/post-create.component";
// import { LoginComponent } from './login/login.component'; // 기존 모형뿐인 로그인

//import { DialogOverviewExampleDialog } from "./post-list/post-list.component";

// 팝업은 라우팅 할 컴포넌트가 없다.
// post-list.ts 에 컴포넌트 로직을 merge 했기 때문.

import { LoginComponent } from "./auth/login/login.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { AuthGuard } from './auth/auth.guard';

import { AfterSearchPostListComponent } from "./after-search-post-list/after-search-post-list.component";

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  // { path:'login', component: LoginComponent },

	{ path: 'api/customers', component: CustomerComponent },
  { path: 'customers', component: CustomerComponent },
  { path: 'customer/add', component: AddCustomerComponent },
  { path: 'customers/:id', component: CustomerDetailsComponent },

  { path: 'post/list', component:PostListComponent },
  { path: 'post/create', component:PostCreateComponent },
  { path: 'edit/:postId', component:PostCreateComponent },

  { path: 'api/comments', component: DialogOverviewExampleDialog },
  { path: 'comment/add', component: DialogOverviewExampleDialog },

  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },

  { path: 'afterSearchPostList', component: AfterSearchPostListComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})

export class AppRoutingModule { }
