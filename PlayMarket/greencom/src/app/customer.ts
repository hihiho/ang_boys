export class Customer {
    _id: string;

    userId: string;
    userPw: string;

    firstname: string;
    lastname: string;
    age: number;
    address: string;
    createDate: string;
}
