import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AuthService } from './auth/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private authService:AuthService){}

  ngOnInit(){

    console.log("//해당 DIV 사이즈 자동조정 ", $("div.a").height());

    if($('div.post-list').height()>$('div.a').height()){
      $('div.a').css("height",$('div.post-list').height()+55 +"px");
    }

    this.authService.autoAuthUser();
  }

};



