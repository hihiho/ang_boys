import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class NowTime{
  origienDate:any = new Date(); // 날짜 얻어옴
  year:any = this.origienDate.getFullYear(); // 연도 추출
  month:any = this.origienDate.getMonth()+1; // 월 추출 // month는 희안하게 0월부터 시작...
  day:any = this.origienDate.getDate(); // 일 추출
  hour:any = this.origienDate.getHours(); // 시간 추출
  minut:any = this.origienDate.getMinutes(); // 분 추출
  seconds:any = this.origienDate.getSeconds(); // 초
  completNowDate:string = this.year + "/" + this.month + "/" + this.day + "/" +this.hour + "시 " + this.minut + "분" + this.seconds + "초"; // 연 월 일 합성

  public getNowTime (){
    console.log("여기는 getNowTime");
    console.log(this.completNowDate);
    return this.completNowDate;
  }
}
