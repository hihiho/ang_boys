import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PostsService } from '../post.service';
import { Post } from '../post';
import { mimeType } from "./mime-type.validator";
import { Location } from '@angular/common';
import { Router } from "@angular/router";

// 생성 시간을 위해 import하여 재활용
import { NowTime } from "../nowTime.service";

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit{

  enteredTitle = "";
  enteredContent = "";
  post:Post;
  isLoading = false;
  form:FormGroup;
  imagePreview:string;
  private mode ="create"; // 생성과 수정의 플래그
  private postId:string;
  submitted = false;

  constructor(
  	public postsService:PostsService,
    public route:ActivatedRoute,
    private location: Location,
    private router: Router,
    public nowTimeRes:NowTime,
  ){ }
  completNowDate:string = this.nowTimeRes.getNowTime();

  ngOnInit(){
    this.form = new FormGroup({

      title: new FormControl(null, {
        validators:[Validators.required, Validators.minLength(3)]
      }),

      content: new FormControl(null,
        {validators:[Validators.required]
      }),

      image: new FormControl(null,{
        validators:[Validators.required],
        asyncValidators:[mimeType]
      }),

      status: new FormControl(null,
        {validators:[Validators.required]
      }),
    });

    this.route.paramMap.subscribe((paramMap:ParamMap)=>{
      if(paramMap.has("postId")){
        this.mode = "edit";
        this.postId = paramMap.get("postId");
        console.log("업데이트 !!"+this.postId);
        this.isLoading = true;
        this.postsService.getPost(this.postId).subscribe(postData =>{
          this.isLoading = false;
          this.post = {
            id:postData._id,
            title:postData.title,
            content:postData.content,
            imagePath:postData.imagePath,

            //추가
            createDate:this.completNowDate,
            status:postData.status,
            coment:postData.coment
          };
          this.form.setValue({
            title:this.post.title,
            content:this.post.content,
            image:this.post.imagePath,

            // 추가
            createDate:this.completNowDate, // 로직의 멤버변수
            status:this.post.status,
            coment:this.post.coment
          });
        });
      }else{
        this.mode = "create";
        this.postId = null;
      }
    });
  }

  onImagePicked(event:Event){
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({image:file});
    this.form.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  onSavePost(){
    if(this.form.invalid){
      return;
    }

    this.isLoading = true;

    if(this.mode === "create"){
      this.postsService.addPost(
        this.form.value.title,
        this.form.value.content,
        this.form.value.image,
        this.completNowDate,
        this.form.value.status,
        this.form.value.coment
        );
    }else{
      this.postsService.updatePost(
        this.postId,
        this.form.value.title,
        this.form.value.content,
        this.form.value.image,
        this.completNowDate,
        this.form.value.status,
        this.form.value.coment
      );
    }

    this.form.reset();
  }

  formHideTrigger(): void{
    this.submitted = true;
  }

  goBack(): void {
    console.log("goBack 들어옴");
    this.location.back();
    this.form.reset();
    console.log("goBack 눌림!!");
  }

  goList(): void {
    console.log("goList 들어옴");
    this.router.navigate(["post/list"]);
    console.log("goBack 눌림!!");
  }

}
