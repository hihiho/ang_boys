import { Component, OnInit, OnDestroy, NgModule } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from "../post";
import { PostsService } from "../post.service";
import { Injectable } from '@angular/core';

// 댓글 시간을 위해 import하여 재활용
import { NowTime } from "../nowTime.service";

// 댓글 팝업
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Comment } from "../comment";
import { CommentService } from "../comment.service";

// 검색 키워드 받아오기
import { HeaderComponent } from "../header/header.component";
import { Router } from '@angular/router';

// post-list 구현부
@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})

@Injectable({providedIn: 'root'})
export class PostListComponent implements OnInit, OnDestroy{

  posts:Post[] = [];
  public isLoading = false;
  private postsSub:Subscription;
  comment: string;
  getTime:any = this.nowTimeRes.getNowTime();
  commentList:Comment[] = [];
  sendSearchKeyword:string = "";
  public headerComponent:HeaderComponent;
  filterPosts:Post[] = [];
  public flag:boolean = true;

  constructor(
    public postsService:PostsService,
    public nowTimeRes:NowTime,
    public dialog: MatDialog,
    public commentService:CommentService,
    private router:Router,
    ) { }

  ngOnInit(){
    this.isLoading = true;
    this.postsService.getPosts();
    this.postsSub = this.postsService.getPostUpdateListener()
      .subscribe((posts: Post[]) => {
        this.isLoading = false;
        this.posts = posts;
      });

    // post별 댓글을 매핑하기 위한 준비
    this.commentService.getComments()
      .subscribe((comments:Comment[]) => {
        this.commentList = comments; // 모든 댓글을 불러와서, 모든글에 다 보인다.
      });

    if(this.flag==true){
      console.log("flag true");
    }else if(this.flag==false){
      console.log("flag false");
    }

  } // ngOnInit

  public search(string:string){
    // 검색 키워드 받아오기
    console.log("post-list / searchKeyword : ", string);
    // this.sendSearchKeyword = this.headerComponent.reqSearchKeyword;
    //this.filterPosts=this.postsService.getPosts();
    // getPosts는 로그가 안찍힌다.....
    this.posts = this.filterPosts;
    console.log("리스트 완성");
    this.router.navigate(["/afterSearchPostList"]);
    // this.router.navigate(["/post/list"]);
  }


  openDialog(postId): void { ///// 댓글 팝업 구현부

    console.log("post id 확인 ",postId);
    // this.postsService.getPost(postId); // 파라미터 id를 구해야 함

    console.log("댓글 입력 버튼 눌림");
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: { // 순수 입력받은 데이터
        target: postId,
        comment: this.comment
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('댓글 입력 취소!!!');
    });
  } // openDialog // 댓글 팝업

  onDelete(postId:string){
    this.postsService.deletePost(postId);
  }

  ngOnDestroy(){
    this.postsSub.unsubscribe();
  }

}




@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: '../popup-coment/dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog implements OnInit {// 댓글 팝업 종료 구현부

  comment = new Comment();
  comments: Comment[];
  // http:HttpClient;
  getTime:any = this.nowTimeRes.getNowTime();

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    private commentService:CommentService,
    // @Inject(MAT_DIALOG_DATA) public data: DialogData,
    @Inject(MAT_DIALOG_DATA) public dialogInput: Comment,
    public nowTimeRes:NowTime,
    ) { }

    ngOnInit(): void {
      // this.getCustomers();
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // newComment(): void {
  //   this.comment = new Comment;
  // }

  addComment(): void {
    console.log("댓글 입력 시도!");
    console.log(this.dialogInput);
    this.comment['target'] = this.dialogInput.target;
    this.comment['content'] = this.dialogInput.content;
    this.comment['createDate'] = this.getTime;

    console.log(this.comment);
    console.log("가공/변환 완료");
    // 입력된 data를 댓글에 입력 하기위해 호출!
    this.save();
    this.dialogRef.close(); // 닫기
  }

  private save():void{
    console.log("댓글 DB저장 시도!!");
    console.log(this.dialogInput);
    console.log(this.comment);

    // 저장 시도!
    //this.commentService = new CommentService(this.http);
    this.commentService.addComment(this.comment).subscribe();

    console.log("subscribe 했나?");
  }

  getCustomers() {
    return this.commentService.getComments()
               .subscribe(
                 i => {
                  console.log(i);
                  this.comments = i
                 }
                );
 }

} // DialogOverviewExampleDialog
