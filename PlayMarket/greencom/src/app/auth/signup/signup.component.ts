import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NowTime } from "../../nowTime.service";


@Component({
  templateUrl:"./signup.component.html",
  styleUrls:["./signup.component.css"]
})
export class SignupComponent implements OnInit{
  isLoading = false;
  creDate:string;

  constructor(
    public authService:AuthService,
    private createDate:NowTime,
    ){ }

  ngOnInit(): void {
    this.creDate = this.createDate.getNowTime();
  }

  onSignup(form:NgForm){
    if(form.invalid){
      return;
    }
    this.isLoading = true;
    this.authService.createUser(
      form.value.email,
      form.value.password,
      form.value.firstname,
      form.value.lastname,
      form.value.age,
      form.value.address,
      this.creDate
      );
  }
}
