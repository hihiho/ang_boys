export interface AuthData{ // interface -> class
  email:string; // id대신 email형식 사용
  password:string;

  firstname?: string;
  lastname?: string;
  age?: number;
  address?: string;

  createDate?: string;
}

// 해당 ts파일을 user.service에서 import하여 사용할 때,
// login시점에는 signup과는 다르게 모든 필드를 사용하지 않으므로,
// 필드를 가변으로 사용하겠다는 의미로 변수명 뒤에 ?를 붙였다.
// 이로써, interface나 class를 다중으로 만들지 않아도 되는 오버로딩이 된 것.

// export class AuthLoginData{ // interface -> class
//   email:string; // id대신 email형식 사용
//   password:string;
// }
