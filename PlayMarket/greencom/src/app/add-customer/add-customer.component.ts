import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

import { Location } from '@angular/common';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})

export class AddCustomerComponent implements OnInit{

  customer = new Customer();
  submitted = false;

  // 아래 생성시간 서비스처리 해야 함
  origienDate:any = new Date(); // 날짜 얻어옴
  year:any = this.origienDate.getFullYear(); // 연도 추출
  month:any = this.origienDate.getMonth()+1; // 월 추출 // month는 희안하게 0월부터 시작...
  day:any = this.origienDate.getDate(); // 일 추출
  completNowDate:any = this.year + "/" + this.month + "/" + this.day; // 연 월 일 합성


  constructor(
    private customerService: CustomerService,
    private location: Location
  ) { }

  ngOnInit():void {  }

  newCustomer(): void {
    this.submitted = false;
    this.customer = new Customer();
    //this.customer.createDate = this.completNowDate;
  }

  addCustomer() {
    this.submitted = true;
    console.log(this.customer);
    this.customer.createDate = this.completNowDate; // 저장하기 직전 생성날자 주입 // schema를 수정
    // 시간 서비스처리 하면서
    // 여기서 받은 오브젝트의 userId를 순회하고 비교해서 .save()를 할지 말지 결정
    this.save(); // schema -> save() -> mongoDB
  }

  goBack(): void {
    this.location.back();
  }

  private save(): void {
    console.log(this.customer);
    this.customerService.addCustomer(this.customer)
        .subscribe();
  }
}
